import React, {Component} from 'react';
import './contentRegister.css';
import data from '../data/data.json';

let $imagePreview = null;
let $removeButton = null;
let $filesDrag = "filesDrag";
let $click = false;
let imgUrlState;

class ContentRegister extends Component {

    constructor(props) {
        super(props);
        this.state = {file: '',imagePreviewUrl: '', imageName:''};
      }
    
      handleSubmit(e) {
        e.preventDefault();
      }
    
      handleImageChange(e) {
        e.preventDefault();
    
        let reader = new FileReader();
        let file = e.target.files[0];
    
        reader.onloadend = () => {
          this.setState({
            file: file,
            imagePreviewUrl: reader.result,
            
          });
        }
        reader.readAsDataURL(file)
        this.setState({
            imageName: file.name
        })  
      }

      removeImg = () => {
         $click = true;
         if ($click == true){
          this.setState({
            imagePreviewUrl:''
          })
          $filesDrag =  "filesDrag";
          $removeButton = {display:'none'};
          $click = false;
        }
      }

    render() {
        let {imagePreviewUrl} = this.state;
        if (imagePreviewUrl) {
          $imagePreview = (<img src={imagePreviewUrl} />);
          $filesDrag =  "filesDrag filesDragHide";
          $removeButton = {display:'flex'};
        } else {
          $imagePreview = data.selectFilesField.DragField;  
        }

        imgUrlState = this.state.imagePreviewUrl;
        this.props.checkImgUrl(imgUrlState);

        return (
            <div className='ContentRegister' style={this.props.contentRegisterStyle}>
                <button className='SelectFiles'>
                <input className="fileInput" id="real-input"
                    type="file" 
                    onChange={(e)=>this.handleImageChange(e)} />
                {data.selectFilesField.buttonName}</button>
                <div className={$filesDrag}>
                    {$imagePreview}
                </div>
                <div className='removeButtonPlace' style={$removeButton}>
                    <button className='removeButton' onClick={this.removeImg} >
                        {data.selectFilesField.buttonRemove}<p className='fileName'>{this.state.imageName}</p>
                    </button>
                </div>
            </div>       
            
        )
    }
}
    
export default ContentRegister;