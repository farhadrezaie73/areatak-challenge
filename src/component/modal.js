import React, {Component} from 'react';
import './modal.css';
import data from '../data/data.json';

var closeButtonClick = false;

class Modal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            countryListStyle: {
                display:'none'
            }
        }
    }
    modalClose = () => {
        this.setState ({
            countryListStyle: {
                display:'none'
            },
            show: false
        });
        closeButtonClick = true;
        this.props.modalExit(closeButtonClick);
    }

    countryListShowClick = () => {
        if (this.state.show == true){
            this.setState ({
                countryListStyle: {
                    display:'none'
                },
                show: false
            });
        }else{
            this.setState ({
                countryListStyle: {
                    display:''
                },
                show: true
            });
      
        }
        
    }

    render() {
        
        return (
            <div className="modalBack" style={this.props.modalShow}>
                <div className="Modal">
            
                <div className="modalNavbar">
                    <button className='Close' onClick={this.modalClose}>&times;</button>
                    <p className='LoginLogOut'>{data.loginlogout}</p>
                </div>
                
                <div className="picField">
                    <div className="pic">
                    <p className="picText">{data.picFieldText}</p>
                    </div>
                </div>

                <div className="modalTextField">
                    <p className="loginText">{data.userRegisterOrLogin}</p>
                    <p className="numberRegister">{data.numberRegister}</p>
                </div>

                <div className="number">
                    <input className="numberInput" dir='ltr' type="tel" name="numberInput" pattern="[0-9]{2} [0-9]{2} [0-9]{4} [0-9]{4}" required placeholder='+98 21 2345 6789'/>
                    <div className='dropCountry'>
                        
                        <button className='countryButton' onClick={this.countryListShowClick}>
                        <i class="fa fa-caret-down card"></i>
                        <img className="flag" src={require('../img/countries-flag' + '/ir.png')}></img>
                        </button>
                        
                        <div className="countryList" style={this.state.countryListStyle}>
                            {
                            Object.keys(data.countries).map(key => {
                                return (
                                <li className="flag flag-list" value={data.countries[key].value}><img className="flag" src={require('../img/countries-flag' + data.countries[key].flag)}></img></li>  
                                );
                            })
                            }
                        </div>
                    </div>
                </div>
                <div className='passwordSendPlace'>
                    <button className='passwordSend-Button'>
                        {data.passwordSend}
                    </button>
                </div>

                </div>
                
          </div>
        )
    }
}

    
export default Modal;










