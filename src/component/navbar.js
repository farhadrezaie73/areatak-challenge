import React, {Component} from 'react';
import './navbar.css';
import data from '../data/data.json';

class Navbar extends Component {
    
    render() {

        return (
            <div className="Navbar">
                <div className="navbarTitle">
                    <h1 className="h1Header">{data.Header}</h1>
                    <img src={require('../img' + data.logo)} className="App-logo" alt="React" />
                </div>
                <div className="Items">
                    {
                    Object.keys(data.navbarItems).map(key => {
                        return (
                        <ul className="ItemsList" >
                            <a href={data.navbarItems[key].url} value={data.navbarItems[key].value} onClick={this.navbarItemClick}>{data.navbarItems[key].title}</a>
                        </ul>
                            
                        );
                    })
                    }
                </div> 
                <div className="navbarPanelRegister">
                    <button className="panelButton" onClick={this.props.modal}>
                        {data.navbarPanel.userPanel}               
                    </button>
                    <button className="registerButton" >
                        {data.navbarPanel.newRegister}
                    </button>
                </div>
              
            </div>
        )
    }
}
    
export default Navbar;





