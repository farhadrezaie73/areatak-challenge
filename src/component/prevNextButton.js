import React, {Component} from 'react';
import './prevNextButton.css';
import data from '../data/data.json';

class PrevNextButton extends Component {

    render() {
        
        return (
            <div className='prevNextField'>
                <button className={this.props.prevclassName} onClick={this.props.prevChange}> {data.prevNext.buttonPrev}</button>
                <button className='prevNext' style={this.props.nextButtonStyle} onClick={this.props.nextChange}>{data.prevNext.buttonNext}</button>      
                <button className='Register-Record' style={this.props.recordButtonStyle}>{data.prevNext.buttonRegisterRecord}</button>  
                <button className='inquiry-Record' style={this.props.inquiryButtonStyle}>{data.prevNext.buttonInquiryRecord}</button>       
            </div>
        )
    }
}
    
export default PrevNextButton;










