import React, {Component} from 'react';

import Informations  from "./informations.js";
import Registration from "./registration.js";
import ContentRegister from "./contentRegister.js";
import ProcessField from "./processField.js";
import Navbar from "./navbar.js";
import ContainerNavbar from "./containerNavbar.js";
import PrevNextButton from "./prevNextButton.js";
import Modal from "./modal.js";

import './App.css';
import data from '../data/data.json';

let $contentRegister = true;
let $information= false;
let $registration= false;
let $contentLicense = false;
let $imagePreviewUrlState = '';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      informationStyle : {
        display : 'none',
      },
      registrationStyle: {
        display: 'none',
      },
      recordButtonStyle: {
        display: 'none',
      },
      inquiryButtonStyle: {
        display: 'none'
      },
      prevclassName: "buttonInActive",
      processRegister:
      {
        registerTextClass: "ProcessText ProcessTextActive",
        registerImg: require('../svg' + data.processText.circleOneActiveImg),
        informationTextClass: "ProcessText",
        informationImg: require('../svg' + data.processText.circleTwoImg),
        confirmTextClass: "ProcessText",
        confirmImg: require('../svg' + data.processText.circleThreeImg),
        lineOne: require('../svg' + data.processText.lineImg),
        lineTwo: require('../svg' + data.processText.lineImg),
      },
      processInquiry:
      {
        RegisterSupplementaryClass: "ProcessText ProcessTextActive",
        RegisterSupplementaryImg: require('../svg' + data.processText.circleOneActiveImg),
        LicenseTextClass: "ProcessText",
        LicenseImg: require('../svg' + data.processText.circleTwoImg),
        inqurayLine: require('../svg' + data.inquiryText.inqurayLine)
      },
      processFieldStyle: {
        inquiryStyle: {
          display: 'none',
        },
        newRegisterStyle: {
            display: ''
        }
      },
      registerActive: true,
      modalShow:{
        display:'none'
      },
      contentRegisterReset: false
    }
  }

  checkImgUrl (imgUrlState){
      $imagePreviewUrlState = imgUrlState
    }

  PrevButtonClick = () => {
    if ($information == true && this.state.registerActive == true){
      this.setState({
        contentRegisterStyle: {
          display: '',
        },
        informationStyle : {
          display: 'none',
        },
        prevclassName: 'buttonInActive',
        processRegister:
          {
            registerTextClass: "ProcessText ProcessTextActive",
            registerImg: require('../svg' + data.processText.circleOneActiveImg),
            informationTextClass: "ProcessText",
            informationImg: require('../svg' + data.processText.circleTwoImg),
            confirmTextClass: "ProcessText",
            confirmImg: require('../svg' + data.processText.circleThreeImg),
            lineOne: require('../svg' + data.processText.lineImg),
            lineTwo: require('../svg' + data.processText.lineImg),
            },
      }); 
      $contentRegister = true;
      $information = false;
    
    }else if ($registration == true  && this.state.registerActive == true) {
      this.setState({
        informationStyle : {
          display: '',
        },
        registrationStyle: {
          display: 'none',
        },
        recordButtonStyle: {
          display: 'none',
        },
        nextButtonStyle: {
          display: '',
        },
        processRegister:
        {
          registerTextClass: "ProcessText ProcessTextActive",
          registerImg: require('../svg' + data.processText.checkImg),
          informationTextClass: "ProcessText ProcessTextActive",
          informationImg: require('../svg' + data.processText.circleTwoActiveImg),
          confirmTextClass: "ProcessText",
          confirmImg: require('../svg' + data.processText.circleThreeImg),
          lineOne: require('../svg' + data.processText.lineCheckImg),
          lineTwo: require('../svg' + data.processText.lineImg),
        } 
        
      }); 
      $information = true;
      $registration = false;
    }else if ($contentLicense == true  && this.state.registerActive == false){
      this.setState({
        informationStyle : {
          display : '',
        },
        contentRegisterStyle: {
          display: 'none',
        },
        processInquiry:
        {
            RegisterSupplementaryClass: "ProcessText ProcessTextActive",
            RegisterSupplementaryImg: require('../svg' + data.processText.circleOneActiveImg),
            LicenseTextClass: "ProcessText",
            LicenseImg: require('../svg' + data.processText.circleTwoImg),
            inqurayLine: require('../svg' + data.inquiryText.inqurayLine)
        },
        inquiryButtonStyle: {
          display: 'none'
        },
        nextButtonStyle: {
          display: '',
        },
        prevclassName: 'buttonInActive',
      }); 

      $information = true;
      $contentLicense = false;
    }
    
  }

  NextButtonClick = () => {
      if ($contentRegister == true && this.state.registerActive == true){
        if ($imagePreviewUrlState == ''){
          return
        }
        this.setState({
          contentRegisterStyle: {
            display: 'none',
          },
          informationStyle : {
            display: '',
          },
          prevclassName: 'prevNext',
          processRegister:
          {
            registerTextClass: "ProcessText ProcessTextActive",
            registerImg: require('../svg' + data.processText.checkImg),
            informationTextClass: "ProcessText ProcessTextActive",
            informationImg: require('../svg' + data.processText.circleTwoActiveImg),
            confirmTextClass: "ProcessText",
            confirmImg: require('../svg' + data.processText.circleThreeImg),
            lineOne: require('../svg' + data.processText.lineCheckImg),
            lineTwo: require('../svg' + data.processText.lineImg),
          } 
 
        });
        $contentRegister = false;
        $information = true;
  
      } else if ($information == true){
        if(this.state.registerActive == true){
          this.setState({
            informationStyle : {
              display : 'none',
            },
            registrationStyle: {
              display: '',
            },
            recordButtonStyle: {
              display: '',
            },
            nextButtonStyle: {
              display: 'none',
            },
            processRegister:
            {
              registerTextClass: "ProcessText ProcessTextActive",
              registerImg: require('../svg' + data.processText.checkImg),
              informationTextClass: "ProcessText ProcessTextActive",
              informationImg: require('../svg' + data.processText.checkImg),
              confirmTextClass: "ProcessText ProcessTextActive",
              confirmImg: require('../svg' + data.processText.circleThreeActiveImg),
              lineOne: require('../svg' + data.processText.lineCheckImg),
              lineTwo: require('../svg' + data.processText.lineCheckImg),
            } 
          }); 
    
          $information = false;
          $registration = true;
        }else{
          this.setState({
            informationStyle : {
              display : 'none',
            },
            contentRegisterStyle: {
              display: '',
            },
            processInquiry:
            {
                RegisterSupplementaryClass: "ProcessText ProcessTextActive",
                RegisterSupplementaryImg: require('../svg' + data.processText.checkImg),
                LicenseTextClass: "ProcessText ProcessTextActive",
                LicenseImg: require('../svg' + data.processText.circleTwoActiveImg),
                inqurayLine: require('../svg' + data.inquiryText.inqurayLine)
            },
            inquiryButtonStyle: {
              display: ''
            },
            nextButtonStyle: {
              display: 'none',
            },
            prevclassName: 'prevNext',
          }); 
    
          $information = false;
          $contentLicense = true;
        }
        
      }
    }
  
  changeSubContainer = ($inquiryStyle, $newRegisterStyle, $registerActive) => {
      if($registerActive == true){
        this.setState({
          processFieldStyle: {
            inquiryStyle : $inquiryStyle,
            newRegisterStyle : $newRegisterStyle
          },
          contentRegisterStyle: {
            display: '',
          },
          informationStyle : {
            display: 'none',
          },
          registrationStyle: {
            display: 'none',
          },
          processRegister:
          {
            registerTextClass: "ProcessText ProcessTextActive",
            registerImg: require('../svg' + data.processText.circleOneActiveImg),
            informationTextClass: "ProcessText",
            informationImg: require('../svg' + data.processText.circleTwoImg),
            confirmTextClass: "ProcessText",
            confirmImg: require('../svg' + data.processText.circleThreeImg),
            lineOne: require('../svg' + data.processText.lineImg),
            lineTwo: require('../svg' + data.processText.lineImg),
          },
          prevclassName: "buttonInActive",
          recordButtonStyle: {
            display: 'none',
          },
          nextButtonStyle: {
            display: '',
          },
          inquiryButtonStyle: {
            display: 'none'
          },
          registerActive: true,
          contentRegisterReset: true
        });
        $contentRegister = true;
        $information = false;
        $registration = false;
        
      }else {
        this.setState ({
            processFieldStyle: {
            inquiryStyle : $inquiryStyle,
            newRegisterStyle : $newRegisterStyle
          },
          contentRegisterStyle: {
            display: 'none',
          },
          informationStyle : {
            display: '',
          },
          registrationStyle: {
            display: 'none',
          },
          prevclassName: 'buttonInActive',
          processInquiry:
          {
            RegisterSupplementaryClass: "ProcessText ProcessTextActive",
            RegisterSupplementaryImg: require('../svg' + data.processText.circleOneActiveImg),
            LicenseTextClass: "ProcessText",
            LicenseImg: require('../svg' + data.processText.circleTwoImg),
            inqurayLine: require('../svg' + data.inquiryText.inqurayLine)
          },
          recordButtonStyle: {
            display: 'none',
          },
          nextButtonStyle: {
            display: '',
          },
          inquiryButtonStyle: {
            display: 'none'
          },
          registerActive: false,
          contentRegisterReset: true
        });
        $information = true;
        $contentLicense = false;
        
      }
    }

    Modal = ($closeButtonClick) => {
      if ($closeButtonClick == true){
        this.setState({
          modalShow: {
            display: 'none'
          }
        });
      }else {
        this.setState({
          modalShow: {
            display: ''
          }
        });
      }
      
    }
    
  render() {
    return (
      <div id='up' className="Content">
          <Navbar modal={this.Modal}></Navbar>
          
          <div className='Title' >
            <h1>{data.title}</h1>
            <p>{data.subtitle}</p>
          </div>

          <Modal modalShow ={this.state.modalShow} modalExit={this.Modal}></Modal>
              
          <div className='Container'>
            <ContainerNavbar changeSubContainer={this.changeSubContainer}></ContainerNavbar>
            <div className="SubContainer">
              <ProcessField newRegister={this.state.processRegister} inquiry={this.state.processInquiry} subContainerNavbarChange={this.state.processFieldStyle}></ProcessField>     
              <ContentRegister contentRegisterStyle={this.state.contentRegisterStyle} checkImgUrl={this.checkImgUrl}></ContentRegister>
              <Informations informationStyle={this.state.informationStyle}></Informations>
              <Registration registrationStyle={this.state.registrationStyle}></Registration> 
              <PrevNextButton nextChange={this.NextButtonClick} prevChange ={this.PrevButtonClick} recordButtonStyle ={this.state.recordButtonStyle} inquiryButtonStyle ={this.state.inquiryButtonStyle} nextButtonStyle={this.state.nextButtonStyle} prevclassName={this.state.prevclassName}></PrevNextButton>

            </div>
          </div>
        </div>
    );
  }
}

export default App;
