import React, {Component} from 'react';
import './informations.css';
import data from '../data/data.json';

class Informations extends Component {

    render() {
        return (
            <div className='information' style={this.props.informationStyle}>
                <p className='information-Header-Text'>{data.information.headerText}</p>
                <div>
                    <form className='Form-Place'>
                      <input className='Name' type="text" name="firstname" placeholder="نام"/>
                      <input className='Name' type="text" name="lastname" placeholder="نام خانوادگی"/>
                      <textarea className='Description' name="Description" placeholder="توضیحات"/>
                    </form>
                </div>
            </div>
        )
    }
}
    
export default Informations;