import React, {Component} from 'react';
import './processField.css';
import data from '../data/data.json';


class ProcessField extends Component {

    render() {
        return (
            <div>
                <div className='Process' style={this.props.subContainerNavbarChange.newRegisterStyle}>
                    <div className='ProcessField one'>
                        <img src={this.props.newRegister.registerImg}></img>
                        <p className = {this.props.newRegister.registerTextClass} > {data.processText.registerText}</p>
                    </div>

                    <div>
                        <img src={this.props.newRegister.lineOne}></img>
                    </div>
                    
                    <div className='ProcessField'>
                        <img src={this.props.newRegister.informationImg}></img>
                        <p className={this.props.newRegister.informationTextClass}> {data.processText.information}</p>
                    </div>

                    <div>
                        <img src={this.props.newRegister.lineTwo}></img>
                    </div>

                    <div className='ProcessField three'>
                        <img src={this.props.newRegister.confirmImg}></img>
                        <p className={this.props.newRegister.confirmTextClass}> {data.processText.ConfirmText}</p>
                    </div>
                </div>

                <div className='Process'  style={this.props.subContainerNavbarChange.inquiryStyle}> 
                    <div className='ProcessField'>
                        <img src={this.props.inquiry.RegisterSupplementaryImg}></img>
                        <p className = {this.props.inquiry.RegisterSupplementaryClass} > {data.inquiryText.RegisterSupplementary}</p>
                    </div>

                    <div>
                        <img src={this.props.inquiry.inqurayLine}></img>
                    </div>

                    <div className='ProcessField license'>
                        <img src={this.props.inquiry.LicenseImg}></img>
                        <p className={this.props.inquiry.LicenseTextClass}> {data.inquiryText.LicenseText}</p>
                    </div>
                </div>
           </div>
        )
    }
}
    
export default ProcessField;



