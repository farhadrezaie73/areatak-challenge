import React, {Component} from 'react';
import './containerNavbar.css';
import data from '../data/data.json';

let $registerActive = true;
let $inquiryStyle;
let $newRegisterStyle;

class ContainerNavbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
          containerRegister: 'ContainerButton ContainerActive',
          containerInquiry: 'ContainerButton',
          nextButton: {
            display: '',
          },
        }
    }

    ContainerRegisterClick = () => {
        this.setState({
          containerRegister: 'ContainerButton ContainerActive',
          containerInquiry: 'ContainerButton',
          });
        $inquiryStyle = {
            display: 'none',
        };
        $newRegisterStyle= {
              display: ''
        };
        $registerActive = true;
        this.props.changeSubContainer($inquiryStyle, $newRegisterStyle, $registerActive);
    }
  
    ContainerInquiryClick = () => {
      
        this.setState({
          containerRegister: 'ContainerButton',
          containerInquiry: 'ContainerButton ContainerActive',
          });
          $inquiryStyle = {
            display: '',
          };
          $newRegisterStyle = {
              display: 'none'
          };
          $registerActive = false;
          this.props.changeSubContainer($inquiryStyle, $newRegisterStyle, $registerActive);          
    }
    
    render() {
        return (
            <div class='ContainerNavbar'>
                <div className='ButtonPlace'>
                    <button className={this.state.containerRegister} onClick={this.ContainerRegisterClick}>
                        {data.contentSections.register}
                    </button>
                </div>
                <div className='ButtonPlace'>
                    <button className={this.state.containerInquiry} onClick={this.ContainerInquiryClick}>
                        {data.contentSections.Inquiry}
                    </button>
                </div>
            </div>
        )
    }
}
    
export default ContainerNavbar;





