import React, {Component} from 'react';
import './registration.css';
import data from '../data/data.json';

class Registration extends Component {

    render() {
        return (
            <div className='information-Registration' style={this.props.registrationStyle}>
            
                <form className='Registration-Form-Place'>
                    <div className='Confirmation-Label'>
                      <lable>{data.Confirmation.Filename}</lable>
                      <input className='Confirmation-Input' type="text" name="Filename"/>
                    </div>
                      
                    <div className='Confirmation-Label'>
                      <lable>{data.Confirmation.Description}</lable>
                      <input className='Confirmation-Input' type="text" name="Description"/>
                    </div>
                      
                    <div className='Confirmation-Label'>
                      <lable>{data.Confirmation.Name}</lable>
                      <input className='Confirmation-Input' type="text" name="Name"/>
                    </div>
                      
                    <div className='Confirmation-Label'>
                      <lable>{data.Confirmation.Family}</lable>
                      <input className='Confirmation-Input' type="text" name="Family"/>
                    </div>

                    <div className='Confirmation-Label'>
                      <lable>{data.Confirmation.Email}</lable>
                      <input className='Confirmation-Input' type="email" name="Email"/>
                    </div>

                    <div className='Confirmation-checkbox-Label'>
                      <input type="checkbox" id="checkbox" name="checkbox"/>
                      <label for="checkbox">ثبت سریع</label>
                    </div>

                  </form>

            </div>
        )
    }
}
    
export default Registration;